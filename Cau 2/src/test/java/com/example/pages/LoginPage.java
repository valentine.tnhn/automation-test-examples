package com.example.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class LoginPage {
    WebDriver driver;

    public LoginPage(WebDriver driver) {
        this.driver = driver;
    }


    By txtEmail = By.xpath("//div[@class='mb-2']//input[@name='username']");
    By txtPassword = By.xpath("//div[@class='mb-2']//input[@name='password']");
    By loginBtn = By.xpath("//span[contains(text(),'Login')]");

    public static By msgError(String type) {
        return By.xpath("//p[contains(text(),'" + type + "')]");
    }

    By checkBox = By.xpath("//label[@class='checkbox']");


    public void userOpenHomePage() {
        this.driver.get("https://phptravels.net/login");
        this.driver.manage().window().maximize();
    }

    public void enterEmail(String email) {
        this.driver.findElement(txtEmail).sendKeys(email);
    }

    public void enterPassword(String password) {
        this.driver.findElement(txtPassword).sendKeys(password);
    }

    public void clickLoginBtn() {
        this.driver.findElement(loginBtn).click();
    }

    public void verifyErrorLoginMsg(String type) {
        this.driver.findElement(msgError(type)).isDisplayed();
    }

    public void clickRememberMe() {
        Boolean isClicked = this.driver.findElement(checkBox).isSelected();
        if (isClicked == false) {
            this.driver.findElement(checkBox).click();
        }
    }
}

