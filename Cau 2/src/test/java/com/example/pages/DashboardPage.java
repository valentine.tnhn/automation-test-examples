package com.example.pages;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class DashboardPage {
    By Dashboard = By.xpath("//h1[contains(text(),'Dashboard')]");
    WebDriver driver;

    public DashboardPage(WebDriver driver) {
        this.driver = driver;
    }

    public void verifyLogin() {
        String msg = driver.findElement(Dashboard).getText();
        Assert.assertEquals(msg, "Dashboard");
    }
}

