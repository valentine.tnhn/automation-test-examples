package com.example.stepDefinitions;


import com.example.pages.DashboardPage;
import com.example.pages.LoginPage;
import io.cucumber.java.After;
import io.cucumber.java.Before;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class LoginStepDef {
    WebDriver driver;
    LoginPage login;
    DashboardPage dashboard;

    @Before
    public void StartTest() {
        System.setProperty("webdriver.chrome.driver", "/Users/mac/Desktop/demo2/src/test/resources/drivers/chromedriver");
        this.driver = new ChromeDriver();
        this.login = new LoginPage(this.driver);
        this.dashboard = new DashboardPage(this.driver);
    }

    @Given("^User open home page$")
    public void userOpenHomePage() {
        login.userOpenHomePage();
    }

    @When("^User enters \"([^\"]*)\" and \"([^\"]*)\" on Login Page$")
    public void userIsAlreadyOnLoginPage(String email, String password) {
        if (email.matches("@(.*)@") && password.matches("@(.*)@")) {
            login.enterEmail(email);
            login.enterPassword(password);
        }
    }

    @And("^User click to login button$")
    public void userClickToLoginButton() throws InterruptedException {
        login.clickLoginBtn();
        Thread.sleep(3000);
    }

    @Then("^Verify login$")
    public void userGoToDashboardAndSeeAlert() {
        dashboard.verifyLogin();
    }

    @After
    public void AfterTest() {
        driver.close();
        driver.quit();
    }

    @Then("^User click checkbox Remember me$")
    public void userClickCheckboxRememberMe() {
        login.clickRememberMe();

    }

    @Then("^Verify login status in case \"([^\"]*)\"$")
    public void verifyLoginStatusInCase(String status) {
        String msgError[] = status.split(",");
        for (int i = 0; i <= msgError.length; i++)
            login.verifyErrorLoginMsg(status);
    }
}

