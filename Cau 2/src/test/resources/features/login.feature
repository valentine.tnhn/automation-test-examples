Feature: Login PHP travels

  Scenario: Login dashboard php travels
    Given User open home page
    When User enters "admin@phptravels.com" and "demoadmin" on Login Page
    And User click to login button
    Then Verify login


  Scenario Outline: Verify placeholder with email and password
    Given User open home page
    When User enters "<email>" and "<password>" on Login Page
    Then User click checkbox Remember me
    Then Verify login status in case "<status>"
    Examples:
      | email             | password | status                                                                            |
      |                   |          | The Email field is required,The Password field is required                        |
      |                   | 123456   | The Email field is required                                                       |
      | someone@gmail.com |          | The Password field is required                                                    |
      | someone           |          | The Email field must contain a valid email address,The Password field is required |
      | Someone           | 1234     | The Email field must contain a valid email address                                |
      | someone@gmail.com | 1234     | Invalid Login Credentials                                                         |

