import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.temporal.TemporalAdjusters;
import java.util.Calendar;
import java.util.Date;
import java.util.Scanner;


public class testExample {
    public static void main(String[] args) throws ParseException {
        System.out.println("Bạn muốn chọn: " +
                "\n 1.Viết hàm tính tổng số ngày từ ngày bắt đầu đến ngày kết thúc" +
                "\n 2.Tính ngày tiếp theo khi có số tháng muốn cộng thêm.");
        Scanner scanner = new Scanner(System.in);
        String number = scanner.nextLine();
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyyMMdd");
        switch (number) {
            case "1":
                System.out.println("Nhập số ngày ban đầu theo dạng yyyyMMdd ");
                String start = scanner.nextLine();
                System.out.println("Nhập số ngày cuối theo dạng yyyyMMdd");
                String end = scanner.nextLine();
                //Chuyển đổi định dạng qua date time
                Date dateStart = simpleDateFormat.parse(start);
                Date endDate = simpleDateFormat.parse(end);
                //Chuyển đổi thời gian qua mili giây
                Long startValues = dateStart.getTime();
                Long endValues = endDate.getTime();
                //khoảng mili giây cách nhau
                Long tmp = Math.abs(startValues - endValues);
                //Chia cho số mili giây trong 1 ngày
                Long result = (tmp / (24 * 60 * 60 * 1000));
                System.out.println("Số ngày chênh lệch: " + result);
                break;
            case "2":
                //Nhập ngày tháng và số tháng muốn thêm
                System.out.println("Nhập ngày ban đầu");
                String firstDate = scanner.nextLine();
                System.out.println("Nhập số tháng");
                int numberOfMonths = scanner.nextInt();

                LocalDate startDate = LocalDate.parse(firstDate, DateTimeFormatter.BASIC_ISO_DATE);
                LocalDate closeDate = startDate.plusMonths(numberOfMonths).with(TemporalAdjusters.lastDayOfMonth());
                String formattedCloseDate = closeDate.format(DateTimeFormatter.BASIC_ISO_DATE);
                System.out.println(formattedCloseDate);
                break;
        }

    }
}

